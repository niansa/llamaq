#ifndef QLLM_H
#define QLLM_H
namespace LM {
class Inference;
}

#include <QObject>

class QLLM : public QObject
{
    Q_OBJECT

    LM::Inference *llm;

    bool wants_stop = false;
    QString weights_path;

public:    
    explicit QLLM(QObject *parent = nullptr);

    void cancel();
    void setWeightsPath(const QString&);

public slots:
    void start();
    void stop();
    void append(const QString& str);
    void generate(const QString& end);

signals:
    void begun();
    void done(bool cancelled);
    void progressReported(float progress);
    void appended();
    void generated(const QString& str);
};

#endif // QLLM_H
