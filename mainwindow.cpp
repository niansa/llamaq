#include "mainwindow.h"
#include "./ui_mainwindow.h"

#include <QScrollBar>
#include <QTimer>



MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    // Set up UI
    ui->setupUi(this);
    ui->progressBar->setHidden(true);
    ui->chatHistory->setHidden(true);
    ui->writingArea->setHidden(true);

    connect(ui->sendBtn, &QPushButton::clicked, this, [this] () {
        auto text = ui->messageBar->text();
        // Make sure message has length
        if (text.length() == 0) return;
        // Add chat message to history
        addChatMessage(userName, text);
        // Clear message bar
        ui->messageBar->clear();
        // Start generating a response
        startResponseGeneration(text);
    });
    connect(ui->setupRunBtn, &QPushButton::clicked, this, [this] () {
        // Get specified settings
        botName = ui->botName->text();
        userName = ui->userName->text();
        // Switch to chat view
        ui->chatHistory->setHidden(false);
        ui->writingArea->setHidden(false);
        ui->setupArea->setHidden(true);
        // Set weights path
        llm.setWeightsPath(ui->weightsPath->text());
        // Start llama and wait for a tiny bit
        startLlm();
        QTimer::singleShot(50, this, [this] () {
            // Pass initial prompt
            initialPrompt(ui->initialPrompt->toPlainText());
            // Add initial chat message
            addChatMessage(botName, "Hello! How can I assist you today?");
        });
    });

    connect(&llm, &QLLM::begun, this, [this] () {
        // Show progress bar and disable send button
        ui->sendBtn->setDisabled(true);
        ui->progressBar->setHidden(false);
        ui->progressBar->setMaximum(0);
    });
    connect(&llm, &QLLM::done, this, [this] (bool cancelled) {
        // Hide progress bar, enable send button and enable setup button
        ui->progressBar->setHidden(true);
        ui->sendBtn->setDisabled(false);
        // Remove user suffix
        if (currentGeneration && !cancelled) {
            auto text = currentGeneration->text();
            text.resize(text.size()-userName.size()-2);
            currentGeneration->setText(text);
        }
    });
    connect(&llm, &QLLM::progressReported, this, [this] (float progress) {
        // Configure and set progress bar value
        ui->progressBar->setMaximum(10000);
        ui->progressBar->setValue(progress*100.f);
    });
    connect(&llm, &QLLM::generated, this, [this] (const QString& token) {
        // If a response generation is currently running, add newly generated token to history
        if (currentGeneration) {
            currentGeneration->setText(currentGeneration->text()+token);
            scrollHistoryToBottom();
        }
    });
}

MainWindow::~MainWindow()
{
    stopLlm();
    delete ui;
}

void MainWindow::startLlm()
{
    llm.moveToThread(&llmThread);
    connect(&llmThread, &QThread::started, &llm, &QLLM::start);
    connect(&llmThread, &QThread::finished, &llm, &QLLM::stop);
    llmThread.setObjectName("llama thread");
    llmThread.start();
}

void MainWindow::stopLlm()
{
    llmThread.quit();
    llm.cancel();
    while (llmThread.isRunning());
}

void MainWindow::initialPrompt(const QString &prompt)
{
    // Append initial prompt as-is
    QMetaObject::invokeMethod(&llm, "append", Qt::QueuedConnection,
                              Q_ARG(QString, prompt));
}

void MainWindow::startResponseGeneration(const QString &message)
{
    // Append empty message to history and set as current one
    currentGeneration = addChatMessage(botName, "");
    // Append message with decorations
    QMetaObject::invokeMethod(&llm, "append", Qt::QueuedConnection,
                              Q_ARG(QString, " "+message+"\n"+botName+":"));
    // Generate response
    QMetaObject::invokeMethod(&llm, "generate", Qt::QueuedConnection,
                              Q_ARG(QString, "\n"+userName+":"));
}

void MainWindow::scrollHistoryToBottom()
{
    // Scroll to bottom in history
    QTimer::singleShot(10, this, [scrollBar = ui->chatHistory->verticalScrollBar()] {
        scrollBar->setValue(scrollBar->maximum());
    });
}

QLabel *MainWindow::addChatMessage(const QString &_username, const QString &_content)
{
    auto message = new QWidget(ui->chatHistoryContents);
    auto messageLayout = new QVBoxLayout(message);
    auto username = new QLabel(message);
    username->setText(_username);
    QFont font;
    font.setPointSize(8);
    font.setBold(true);
    username->setFont(font);
    username->setTextFormat(Qt::PlainText);

    messageLayout->addWidget(username);

    auto messageContentLayout = new QHBoxLayout();
    auto horizontalSpacer = new QSpacerItem(4, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

    messageContentLayout->addItem(horizontalSpacer);

    auto content = new QLabel(message);
    content->setText(_content);
    content->setTextFormat(Qt::MarkdownText);
    content->setWordWrap(true);
    content->setOpenExternalLinks(true);
    content->setTextInteractionFlags(Qt::TextInteractionFlag::TextSelectableByMouse);

    messageContentLayout->addWidget(content);

    messageLayout->addLayout(messageContentLayout);

    ui->verticalLayout_2->addWidget(message);

    scrollHistoryToBottom();

    return content;
}
