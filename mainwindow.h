#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include "qllm.h"

#include <QMainWindow>
#include <QThread>
#include <QLabel>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

    QLLM llm;
    QThread llmThread;
    QString botName;
    QString userName;
    QLabel *currentGeneration = nullptr;

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    void startLlm();
    void stopLlm();
    void initialPrompt(const QString& prompt);
    void startResponseGeneration(const QString& message);

    void scrollHistoryToBottom();
    QLabel *addChatMessage(const QString& username, const QString& content);
};
#endif // MAINWINDOW_H
