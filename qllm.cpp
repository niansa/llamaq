#include "qllm.h"

#include <string>
#include <string_view>
#include <justlm.hpp>



QLLM::QLLM(QObject *parent)
    : QObject{parent}
{

}

void QLLM::start()
{
    emit begun();
    LM::Inference::Params params;
    params.use_mlock = false;
    llm = new LM::Inference(weights_path.toStdString(), params);
    emit done(false);
}

void QLLM::stop()
{
    delete llm;
}

void QLLM::append(const QString &str)
{
    emit begun();
    wants_stop = false;
    llm->append(str.toStdString(), [this] (float progress) {
        emit appended();
        emit progressReported(progress);
        return !wants_stop;
    });
    emit done(wants_stop);
}

void QLLM::generate(const QString &end)
{
    emit begun();
    auto end_std = end.toStdString();
    wants_stop = false;
    llm->run(end_std, [this] (const char *generated_token) {
        emit generated(generated_token);
        return !wants_stop;
    });
    emit done(wants_stop);
}

void QLLM::cancel()
{
    wants_stop = true;
}

void QLLM::setWeightsPath(const QString &path)
{
    weights_path = path;
}
